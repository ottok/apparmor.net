---
title: AppArmor 2.13.8 released
date: 2023-05-24
---

AppArmor 2.13.8 is a maintenance release of the user space components of the AppArmor security project. The kernel portion of the project is maintained and pushed separately.

This version of the userspace should work with all kernel versions from
2.6.15 and later (some earlier version of the kernel if they have the
apparmor patches applied).

The kernel portion of the project is maintained and pushed separately.

# Important Note

This release fixes [CVE-2016-1585](https://bugs.launchpad.net/apparmor/+bug/1597017). If you are looking at back porting individual patches instead of pulling in the whole release the critical patches were back ported from [MR:333](https://gitlab.com/apparmor/apparmor/-/merge_requests/333), please contact the [apparmor mailing list](mailto:apparmor@lists.ubuntu.com) or if communications must be private security@apparmor.net if you need assistance.


# Obtaining the Release

There are two ways to obtain this release either through gitlab or a tarball in launchpad. 

**Important note:** the gitlab release tarballs differ from the launchpad release tarballs. The launchpad release tarball has a couple processing steps already performed:

* libapparmor `autogen.sh` is already done, meaning distros only need to use ./configure in their build setup
* the docs for everything but libapparmor have already been built

### gitlab
- https://gitlab.com/apparmor/apparmor/-/releases/v2.13.8

### Launchpad
  -   <https://launchpad.net/apparmor/2.13/2.13.8/+download/apparmor-2.13.8.tar.gz>
  -   sha256sum: ???
  -   signature: <https://launchpad.net/apparmor/2.13/2.13.8/+download/apparmor-2.13.8.tar.gz.asc>

# Changes in this Release

These release notes cover all changes between 2.13.7 (b51a2d27) and 2.13.8 (a0df1428) on [apparmor-2.13 branch](https://gitlab.com/apparmor/apparmor/tree/apparmor-2.13).

## Library
- add scanner support for dbus method ([MR:958](https://gitlab.com/apparmor/apparmor/-/merge_requests/958), [HUBMR:286](https://github.com/bus1/dbus-broker/pull/286))

## Policy Compiler (a.k.a apparmor_parser)
- added missing kernel mount options ([MR:1005](https://gitlab.com/apparmor/apparmor/-/merge_requests/1005), [MR:1034](https://gitlab.com/apparmor/apparmor/-/merge_requests/1034))
- Fix mount rules encoding ([MR:1029](https://gitlab.com/apparmor/apparmor/-/merge_requests/1029), [MR:333](https://gitlab.com/apparmor/apparmor/-/merge_requests/333), [LP:1597017](https://bugs.launchpad.net/bugs/1597017))
- Fix mode not being printed when debugging AF_UNIX socket rules. ([MR:979](https://gitlab.com/apparmor/apparmor/-/merge_requests/979))
- Fix spacing when printing out AF_UNIX addresses ([MR:978](https://gitlab.com/apparmor/apparmor/-/merge_requests/978))

## Utils
- Fix error when choosing named exec with plane profile names ([MR:1013](https://gitlab.com/apparmor/apparmor/-/merge_requests/1013), [AABUG:314](https://gitlab.com/apparmor/apparmor/-/issues/314))
- log parsing fixes ([MR:959](https://gitlab.com/apparmor/apparmor/-/merge_requests/959))

## Policy

#### abstractions
- base
  - allow reading tzdata ICU zoneinfo DB ([MR:1007](https://gitlab.com/apparmor/apparmor/-/merge_requests/1007))
- freedesktop.org
  - allow custom cursors ([MR:1008](https://gitlab.com/apparmor/apparmor/-/merge_requests/1008))
- openssl
  - allow reading /etc/ssl/openssl-*.cnf ([MR:984](https://gitlab.com/apparmor/apparmor/-/merge_requests/984), [BOO:1207911](https://bugzilla.opensuse.org/show_bug.cgi?id=1207911))
- nvidia
  - add new cache directory ([MR:982](https://gitlab.com/apparmor/apparmor/-/merge_requests/982))
  - allow reading @{pid}/comm ([MR:954](https://gitlab.com/apparmor/apparmor/-/merge_requests/954))

#### profiles
- nscd
  - add permission to allow supporting unscd ([MR:1031](https://gitlab.com/apparmor/apparmor/-/merge_requests/1031))
  - allow using systemd-userdb ([MR:977](https://gitlab.com/apparmor/apparmor/-/merge_requests/977))
- syslogd
  - allow reading /dev/kmsg ([MR:1003](https://gitlab.com/apparmor/apparmor/-/merge_requests/1003), [AABUG:307](https://gitlab.com/apparmor/apparmor/-/issues/307))
- nvidia_modprobe
  - update for driver families and /sys path ([MR:983](https://gitlab.com/apparmor/apparmor/-/merge_requests/983))
- postfix-tlsmgr
  - allow reading openssl.cnf ([MR:981](https://gitlab.com/apparmor/apparmor/-/merge_requests/981))
- smbd
  - allow reading /var/lib/nscd/netgroup ([MR:948](https://gitlab.com/apparmor/apparmor/-/merge_requests/948))
- lsb_release
  - allow cat and cut ([MR:953](https://gitlab.com/apparmor/apparmor/-/merge_requests/953))

## Tests
- expand mount tests ([MR:1006](https://gitlab.com/apparmor/apparmor/-/merge_requests/1006), [MR:1033](https://gitlab.com/apparmor/apparmor/-/merge_requests/1033), [MR:1035](https://gitlab.com/apparmor/apparmor/-/merge_requests/1035))
- fix failure on older versions of Make ([MR:1025](https://gitlab.com/apparmor/apparmor/-/merge_requests/1025). [MR:639](https://gitlab.com/apparmor/apparmor/-/merge_requests/639))
- Support rule qualifiers in regression tests ([MR:1020](https://gitlab.com/apparmor/apparmor/-/merge_requests/1020))
- fix af_unix tests for v8 networking. ([MR:893](https://gitlab.com/apparmor/apparmor/-/merge_requests/893))
- fix test failure due to mmap semantic changes (0f0e268b)
