---
title: AppArmor 3.0.10 released
date: 2023-05-24
---

AppArmor 3.0.10 is a maintenance release of the user space components of the AppArmor security project. The kernel portion of the project is maintained and pushed separately.

This version of the userspace should work with all kernel versions from
2.6.15 and later (some earlier version of the kernel if they have the
apparmor patches applied).

The kernel portion of the project is maintained and pushed separately.

# Important Note

This release fixes [CVE-2016-1585](https://bugs.launchpad.net/apparmor/+bug/1597017). If you are looking at back porting individual patches instead of pulling in the whole release the critical patches were backported as commit 262fd11359432888292952e5ed29bead5ace16f0 from [MR:333](https://gitlab.com/apparmor/apparmor/-/merge_requests/333), please contact the [apparmor mailing list](mailto:apparmor@lists.ubuntu.com) or if communications must be private security@apparmor.net if you need assistance.

# Obtaining the Release

There are two ways to obtain this release either through gitlab or a tarball in launchpad. 

**Important note:** the gitlab release tarballs differ from the launchpad release tarballs. The launchpad release tarball has a couple processing steps already performed:

* libapparmor `autogen.sh` is already done, meaning distros only need to use ./configure in their build setup
* the docs for everything but libapparmor have already been built

### gitlab
- https://gitlab.com/apparmor/apparmor/-/releases/v3.0.10

### Launchpad
  -   <https://launchpad.net/apparmor/3.0/3.0.10/+download/apparmor-3.0.10.tar.gz>
  -   sha256sum: 4caa77daabf36de403941c7947d920d08fb30f17785a94ec92da62d6d6a0ef2d
  -   signature: <https://launchpad.net/apparmor/3.0/3.0.10/+download/apparmor-3.0.10.tar.gz.asc>
  -    sha256sum: 87f9bbca467a2e627bab1c6eb8866b1c202baf13472c561caa0f40b636a34684
# Changes in this Release

These release notes cover all changes between 3.0.9 (af9d04d2) and 3.0.10 (090ed4185ca63220cd492f32612b73386806e5d2) on [apparmor-3.0 branch](https://gitlab.com/apparmor/apparmor/tree/apparmor-3.0).

## Library
- Ignore 'x' in mixed file mode log events ([MR:1001](https://gitlab.com/apparmor/apparmor/-/merge_requests/1001), [AABUG:303](https://gitlab.com/apparmor/apparmor/-/issues/303))

## Policy Compiler (a.k.a apparmor_parser)
- added missing kernel mount options ([MR:1005](https://gitlab.com/apparmor/apparmor/-/merge_requests/1005), [MR:1028](https://gitlab.com/apparmor/apparmor/-/merge_requests/1028))
- Fix mount rules encoding ([MR:333](https://gitlab.com/apparmor/apparmor/-/merge_requests/333))
- fix definitely and possibly lost memory leaks ([MR:992](https://gitlab.com/apparmor/apparmor/-/merge_requests/992))

## Utils
- Fix error when choosing named exec with plane profile names ([MR:1013](https://gitlab.com/apparmor/apparmor/-/merge_requests/1013), [AABUG:314](https://gitlab.com/apparmor/apparmor/-/issues/314))
- Ignore 'x' in mixed file mode log events ([MR:1001](https://gitlab.com/apparmor/apparmor/-/merge_requests/1001), [AABUG:303](https://gitlab.com/apparmor/apparmor/-/issues/303))
  - Fix json output ([MR:1036](https://gitlab.com/apparmor/apparmor/-/merge_requests/1036), [AABUG:964](https://gitlab.com/apparmor/apparmor/-/merge_requests/964))

## Policy

#### abstractions
- [3.x] several fixes for samba-related profiles and the kerberos abstraction ([MR:991](https://gitlab.com/apparmor/apparmor/-/merge_requests/991))
- freedesktop.org
  - allow custom cursors ([MR:1008](https://gitlab.com/apparmor/apparmor/-/merge_requests/1008))
- base
  - allow reading tzdata ICU zoneinfo DB ([MR:1007](https://gitlab.com/apparmor/apparmor/-/merge_requests/1007))

#### profiles
- [3.x] several fixes for samba-related profiles and the kerberos abstraction ([MR:991](https://gitlab.com/apparmor/apparmor/-/merge_requests/991))
- Sync apparmor-3.0 profiles with master ([MR:1009](https://gitlab.com/apparmor/apparmor/-/merge_requests/1009))
- nscd
  - add permission to allow supporting unscd ([MR:1031](https://gitlab.com/apparmor/apparmor/-/merge_requests/1031))
- syslogd
  - allow reading /dev/kmsg ([MR:1003](https://gitlab.com/apparmor/apparmor/-/merge_requests/1003), [AABUG:307](https://gitlab.com/apparmor/apparmor/-/issues/307))

## Tests
- expand mount tests ([MR:1006](https://gitlab.com/apparmor/apparmor/-/merge_requests/1006), [MR:1023](https://gitlab.com/apparmor/apparmor/-/merge_requests/1023), [MR:1035](https://gitlab.com/apparmor/apparmor/-/merge_requests/1035))
- Support rule qualifiers in regression tests ([MR:1019](https://gitlab.com/apparmor/apparmor/-/merge_requests/1019))
- fix af_unix tests for v8 networking. ([MR:893](https://gitlab.com/apparmor/apparmor/-/merge_requests/893))
- fix test failure due to mmap semantic changes (0b85c036)
- fix profile generation for dbus test (a03acd0f)
- add write permission to output on dbus test profile (f9349fe4)
- force dbus-daemon to generate an abstract socket ([MR:999](https://gitlab.com/apparmor/apparmor/-/merge_requests/999))
