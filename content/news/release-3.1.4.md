---
title: AppArmor 3.1.4 released
date: 2023-05-24
---

AppArmor 3.1.4 is a bug fix release of the user space components of the AppArmor security project. The kernel portion of the project is maintained and pushed separately.

This version of the userspace should work with all kernel versions from
2.6.15 and later (some earlier version of the kernel if they have the
apparmor patches applied).

# Important Note

This release fixes [CVE-2016-1585](https://bugs.launchpad.net/apparmor/+bug/1597017). If you are looking at back porting individual patches instead of pulling in the whole release the critical patches were backported as commit aff29ef0ee88e18db74a364e7dca1b4c0fa95e47 from [MR:333](https://gitlab.com/apparmor/apparmor/-/merge_requests/333), please contact the [apparmor mailing list](mailto:apparmor@lists.ubuntu.com) or if communications must be private security@apparmor.net if you need assistance.

# Obtaining the Release

There are two ways to obtain this release either through gitlab or a tarball in launchpad. 

**Important note:** the gitlab release tarballs differ from the launchpad release tarballs. The launchpad release tarball has a couple processing steps already performed:

* libapparmor `autogen.sh` is already done, meaning distros only need to use ./configure in their build setup
* the docs for everything but libapparmor have already been built

### gitlab
- https://gitlab.com/apparmor/apparmor/-/releases/v3.1.4

### Launchpad
  -   <https://launchpad.net/apparmor/3.1/3.1.4/+download/apparmor-3.1.4.tar.gz>
  -   sha256sum: 6bee0c3941836dae2c635fe82f09b666123fcac16563aa0fedf4a63c22b91f40
  -   signature: <https://launchpad.net/apparmor/3.1/3.1.4/+download/apparmor-3.1.4.tar.gz.asc>
  -   signature sha256sum: 9b41aa4bf8a7c20ceb208af8140164a28fe7ca2cb137ee3fe6d7392f2bf2dae1

# Changes in this Release

These release notes cover all changes between 3.1.3 (c8eefe44) ) and 3.1.4 (e5a3c03357df1c279a529f8bd8ac0b82e26b427f) on the [apparmor-3.1 branch](https://gitlab.com/apparmor/apparmor/tree/apparmor-3.1.

## Library
- Ignore 'x' in mixed file mode log events ([MR:1001](https://gitlab.com/apparmor/apparmor/-/merge_requests/1001), [AABUG:303](https://gitlab.com/apparmor/apparmor/-/issues/303))

## Policy Compiler (a.k.a apparmor_parser)
- added missing kernel mount options ([MR:1005](https://gitlab.com/apparmor/apparmor/-/merge_requests/1005), [MR:1026](https://gitlab.com/apparmor/apparmor/-/merge_requests/1026))
- Fix mount rules encoding ([MR:333](https://gitlab.com/apparmor/apparmor/-/merge_requests/333))
- fix definitely and possibly lost memory leaks ([MR:992](https://gitlab.com/apparmor/apparmor/-/merge_requests/992))

## Utils
- Fix error when choosing named exec with plane profile names ([MR:1013](https://gitlab.com/apparmor/apparmor/-/merge_requests/1013), [AABUG:314](https://gitlab.com/apparmor/apparmor/-/issues/314))
- Ignore 'x' in mixed file mode log events ([MR:1001](https://gitlab.com/apparmor/apparmor/-/merge_requests/1001), [AABUG:303](https://gitlab.com/apparmor/apparmor/-/issues/303))
- aa-status
  - Fix json output ([MR:1036](https://gitlab.com/apparmor/apparmor/-/merge_requests/1036), [AABUG:964](https://gitlab.com/apparmor/apparmor/-/merge_requests/964))

## Policy

#### abstractions
- several fixes for samba-related profiles and the kerberos abstraction ([MR:991](https://gitlab.com/apparmor/apparmor/-/merge_requests/991))
- base
  - allow reading tzdata ICU zoneinfo DB ([MR:1007](https://gitlab.com/apparmor/apparmor/-/merge_requests/1007))
- freedesktop.org
  - allow custom cursors ([MR:1008](https://gitlab.com/apparmor/apparmor/-/merge_requests/1008))

#### profiles
- Sync apparmor-3.1 profiles with master ([MR:1010](https://gitlab.com/apparmor/apparmor/-/merge_requests/1010))
- several fixes for samba-related profiles and the kerberos abstraction ([MR:991](https://gitlab.com/apparmor/apparmor/-/merge_requests/991))
- nscd
  - add permission to allow supporting unscd ([MR:1031](https://gitlab.com/apparmor/apparmor/-/merge_requests/1031))
- syslogd
  - allow reading /dev/kmsg ([MR:1003](https://gitlab.com/apparmor/apparmor/-/merge_requests/1003), [AABUG:307](https://gitlab.com/apparmor/apparmor/-/issues/307))

## Tests                                                                         
- check extra profiles for local/ includes ([MR:1012](https://gitlab.com/apparmor/apparmor/-/merge_requests/1012), [MR:1027](https://gitlab.com/apparmor/apparmor/-/merge_requests/1027))
- expand mount tests ([MR:1006](https://gitlab.com/apparmor/apparmor/-/merge_requests/1006), [MR:1022](https://gitlab.com/apparmor/apparmor/-/merge_requests/1022), [MR:1035](https://gitlab.com/apparmor/apparmor/-/merge_requests/1035))
- Support rule qualifiers in regression tests ([MR:1018](https://gitlab.com/apparmor/apparmor/-/merge_requests/1018))
- fix af_unix tests for v8 networking. ([MR:893](https://gitlab.com/apparmor/apparmor/-/merge_requests/893))
- force dbus-daemon to generate an abstract socket ([MR:999](https://gitlab.com/apparmor/apparmor/-/merge_requests/999))

