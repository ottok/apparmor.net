---
title: AppArmor 3.1.3 released
date: 2023-02-27
---

AppArmor 3.1.3 is a bug fix release of the user space components of the AppArmor security project. The kernel portion of the project is maintained and pushed separately.

This version of the userspace should work with all kernel versions from
2.6.15 and later (some earlier version of the kernel if they have the
apparmor patches applied).


# Obtaining the Release

There are two ways to obtain this release either through gitlab or a tarball in launchpad. 

**Important note:** the gitlab release tarballs differ from the launchpad release tarballs. The launchpad release tarball has a couple processing steps already performed:

* libapparmor `autogen.sh` is already done, meaning distros only need to use ./configure in their build setup
* the docs for everything but libapparmor have already been built

### gitlab
- https://gitlab.com/apparmor/apparmor/-/releases/v3.1.3

### Launchpad
  -   <https://launchpad.net/apparmor/3.1/3.1.3/+download/apparmor-3.1.3.tar.gz>
  -   sha256sum: b8ffac45d0b283afe574ddd5542142ba1a62c354df110cff862b99201f29c843
  -   signature: <https://launchpad.net/apparmor/3.1/3.1.3/+download/apparmor-3.1.3.tar.gz.asc>
  -   signature sha256sum: c31ba39f3f7e23b6a3f7f301435fb9fa5958807ab477398a5c803e0d8dd18f67

# Changes in this Release

These release notes cover all changes between 3.1.2 (1fe80c0f85db4a67771ea506b1ae6d5626d474b1) ) and 3.1.3 (c8eefe440cd14e49424d40c8ee1bd2f2193b3cfc) on the [apparmor-3.1 branch](https://gitlab.com/apparmor/apparmor/tree/apparmor-3.1).


## Library
- add support for "class" field in logparsing
- add support for "requested" and "denied" fields in logparsing
- add scanner support for dbus "method" field ([MR:958](https://gitlab.com/apparmor/apparmor/-/merge_requests/958), [HUBMR:286](https://github.com/bus1/dbus-broker/pull/286))


## Policy Compiler (a.k.a apparmor_parser)
- Fix mode not being printed when debugging AF_UNIX socket rules. ([MR:979](https://gitlab.com/apparmor/apparmor/-/merge_requests/979))
- Fix spacing when printing out AF_UNIX addresses ([MR:978](https://gitlab.com/apparmor/apparmor/-/merge_requests/978))
- Fix invalid reference to transitions when building the chfa ([MR:956](https://gitlab.com/apparmor/apparmor/-/merge_requests/956), [AABUG:290](https://gitlab.com/apparmor/apparmor/-/issues/290))


## Bin Utils
- aa-status
  - Fix malformed json output with unconfined processes ([MR:964](https://gitlab.com/apparmor/apparmor/-/merge_requests/964), [AABUG:295](https://gitlab.com/apparmor/apparmor/-/issues/295))


## Utils
- Fix log parsing crash due to bad event ([MR:959](https://gitlab.com/apparmor/apparmor/-/merge_requests/959))
- Fix AttributeError caused by Python 3 migration ([MR:986](https://gitlab.com/apparmor/apparmor/-/merge_requests/986))
- Replace mutable default arguments in utils ([MR:986](https://gitlab.com/apparmor/apparmor/-/merge_requests/986))
- Add missing comma to tuple ([MR:986](https://gitlab.com/apparmor/apparmor/-/merge_requests/986))
- Replace mutable default arguments in tests ([MR:986](https://gitlab.com/apparmor/apparmor/-/merge_requests/986))
- Include profile name in error message on directory exec ([MR:949](https://gitlab.com/apparmor/apparmor/-/merge_requests/949), [AABUG:285](https://gitlab.com/apparmor/apparmor/-/issues/285))
- Catch PermissionError when trying to write a profile ([MR:946](https://gitlab.com/apparmor/apparmor/-/merge_requests/946), [AABUG:282](https://gitlab.com/apparmor/apparmor/-/issues/282))


## Policy

#### abstractions
  - Add abstractions/groff with lots of groff/nroff helpers ([MR:973](https://gitlab.com/apparmor/apparmor/-/merge_requests/973), [BOO:1065388](https://bugzilla.opensuse.org/show_bug.cgi?id=1065388))
- audio
  - Add access to pipewire client.conf ([MR:970](https://gitlab.com/apparmor/apparmor/-/merge_requests/970), [LP:2003702](https://bugs.launchpad.net/bugs/2003702))
- base
  - Allow access to possible cpus for glibc-2.36 ([AABUG:267](https://gitlab.com/apparmor/apparmor/-/issues/267), [LP:1989073](https://bugs.launchpad.net/bugs/1989073))
- crypto
  - allow access to hwf.deny ([MR:961](https://gitlab.com/apparmor/apparmor/-/merge_requests/961))
- openssl
    - allow reading /etc/ssl/openssl-*.cnf ([MR:984](https://gitlab.com/apparmor/apparmor/-/merge_requests/984), [BOO:1207911](https://bugzilla.opensuse.org/show_bug.cgi?id=1207911))
- nameservice
  - Adds WSL programmatic management of /etc/resolv.conf ([MR:935](https://gitlab.com/apparmor/apparmor/-/merge_requests/935))
- nvidia
  - add new cache directory ([MR:982](https://gitlab.com/apparmor/apparmor/-/merge_requests/982))
  - allow reading @{pid}/comm ([MR:954](https://gitlab.com/apparmor/apparmor/-/merge_requests/954))
- nvidia_modprobe
  - update for driver families and /sys path ([MR:983](https://gitlab.com/apparmor/apparmor/-/merge_requests/983))
- samba
  - allow modifying /var/cache/samba/*.tdb ([MR:988](https://gitlab.com/apparmor/apparmor/-/merge_requests/988))
- ssl_certs
  - allow access to all entries in pki/trust/ ([MR:961](https://gitlab.com/apparmor/apparmor/-/merge_requests/961))
- ubuntu-helpers
  - Fix: Opening links with Brave ([MR:957](https://gitlab.com/apparmor/apparmor/-/merge_requests/957), [AABUG:292](https://gitlab.com/apparmor/apparmor/-/issues/292))


#### profiles
- Ensure all profiles in have optional local include + comment ([MR:974](https://gitlab.com/apparmor/apparmor/-/merge_requests/974))
- avahi-daemon
  - needs attach_disconnected ([MR:960](https://gitlab.com/apparmor/apparmor/-/merge_requests/960))
- dnsmasq
  - add Waydroid pid file ([MR:969](https://gitlab.com/apparmor/apparmor/-/merge_requests/969))
  - Allow reading /sys/devices/system/cpu/possible ([MR:917](https://gitlab.com/apparmor/apparmor/-/merge_requests/917), [BOO:1202849](https://bugzilla.opensuse.org/show_bug.cgi?id=1202849))
- firefox.sh
  - Adjust local include to match master ([MR:976](https://gitlab.com/apparmor/apparmor/-/merge_requests/976))
- lsb_release
  - allow cat and cut ([MR:953](https://gitlab.com/apparmor/apparmor/-/merge_requests/953))
-  nscd
   - allow using systemd-userdb ([MR:977](https://gitlab.com/apparmor/apparmor/-/merge_requests/977))
- postfix-tlsmgr
  - allow reading openssl.cnf ([MR:981](https://gitlab.com/apparmor/apparmor/-/merge_requests/981))
- samba*
  - allow access to pid files directly in /run/ ([MR:988](https://gitlab.com/apparmor/apparmor/-/merge_requests/988))
- smbd
  - allow reading /var/lib/nscd/netgroup ([MR:948](https://gitlab.com/apparmor/apparmor/-/merge_requests/948))


## Tests
- fix bogon patch characters in Makefile ([MR:963](https://gitlab.com/apparmor/apparmor/-/merge_requests/963))
- add dbus-broker support on regression tests ([MR:965](https://gitlab.com/apparmor/apparmor/-/merge_requests/965))

