---
title: AppArmor 4.0 alpha1 released
date: 2023-07-11
---

AppArmor 4.0 alpha1 is a major new release of the AppArmor that is in development.

Apprmor 4.0 alpha1 is a bridge release between older AppArmor 3.x policy and the newer AppArmor 4 style policy which introduces several new features that are not backwards compatible. As such AppArmor 4.0 alpha1 will be a short lived release, and will not receive long term support. The following AppArmor 4.1 feature release is planned to be a regular release, please take this into account when including AppArmor 4.0 alpha1 into a distro release.

Some features will work with older kernels but many of the features in AppArmor 4.0 alpha1 with require a development kernel.

The kernel portion of the project is maintained and pushed separately.

AppArmor 4.0 alpha1 contains all bug fixes and policy updates from AppArmor 3.1

# Obtaining the Release

This release can be obtained through gitlab.

### gitlab
- https://gitlab.com/apparmor/apparmor/-/releases/v4.0.0-alpha1

# Highlighted new features in alpha1

## New Profile Flag
- unconfined
- debug

## New Mediation Rules
- [fine grain posix mqueue mediation](https://gitlab.com/apparmor/apparmor/-/wikis/mqueue-rules)
- [user ns mediation](https://gitlab.com/apparmor/apparmor/-/wikis/unprivileged_userns_restriction)
- [io_uring mediation](https://gitlab.com/apparmor/apparmor/-/wikis/io_uring-rules)
  - sqpoll and override_creds (cmd is still a wip)

## utils
- aa-status
  - ability to filter output
- aa-load
  - new utility for loading binary (cache) policy without the parser, can be used by non-systemd systems to do cache loads.

## parser
- no longer require root permissions. Will still require privilege to load policy

## Policy
- update abi references to 4.0

## misc
- dbus-broker
  - regression test integration