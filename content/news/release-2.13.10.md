---
title: AppArmor 2.13.10 released
date: 2023-06-21
---
AppArmor 2.13.10 is a bug fix release of the user space components of the AppArmor security project. The kernel portion of the project is maintained and pushed separately.

This version of the userspace should work with all kernel versions from
2.6.15 and later (some earlier version of the kernel if they have the
apparmor patches applied).

# Important Note

This release fixes a regression in 2.13.9 introduced by its fix to mount rule generations.

# Obtaining the Release

There are two ways to obtain this release either through gitlab or a tarball in launchpad. 

**Important note:** the gitlab release tarballs differ from the launchpad release tarballs. The launchpad release tarball has a couple processing steps already performed:

* libapparmor `autogen.sh` is already done, meaning distros only need to use ./configure in their build setup
* the docs for everything but libapparmor have already been built

### gitlab
- https://gitlab.com/apparmor/apparmor/-/releases/v2.13.10

### Launchpad
  -   <https://launchpad.net/apparmor/2.13/2.13.10/+download/apparmor-2.13.10.tar.gz>
  -   sha256sum: a896db78f5ba2489969834e58219a9f03ebf5ae61c234a1af530d926bddd33d1
  -   signature: <https://launchpad.net/apparmor/2.13/2.13.10/+download/apparmor-2.13.10.tar.gz.asc>
  -   signature sha256sum: 88a681f5d794b78c505f86c222ed91f33bdc1acd0f5a06771b3891981a8ac85b

# Changes in this Release

These release notes cover all changes between 2.13.9 (tag v2.13.9) ) and 2.13.10 (tag v2.13.10) on the [apparmor-2.13 branch](https://gitlab.com/apparmor/apparmor/tree/apparmor-2.13).


## Policy Compiler (a.k.a apparmor_parser)
- fix rule flag generation change_mount type rule ([MR:1054](https://gitlab.com/apparmor/apparmor/-/merge_requests/1054), [BOO:1211989](https://bugzilla.opensuse.org/show_bug.cgi?id=1211989), Fixes: [LP:2023814](https://bugs.launchpad.net/bugs/2023814))


## Policy

#### abstractions
- base
  - Add transparent hugepage support ([MR:1050](https://gitlab.com/apparmor/apparmor/-/merge_requests/1050))
- authentication
  -  Add GSSAPI mechanism modules config ([MR:1049](https://gitlab.com/apparmor/apparmor/-/merge_requests/1049))
