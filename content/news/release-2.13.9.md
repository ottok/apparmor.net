---
title: AppArmor 2.13.9 released
date: 2023-06-09
---
AppArmor 2.13.9 is a maintenance release of the user space components of the AppArmor security project. The kernel portion of the project is maintained and pushed separately.

This version of the userspace should work with all kernel versions from
2.6.15 and later (some earlier version of the kernel if they have the
apparmor patches applied).

The kernel portion of the project is maintained and pushed separately.

# Important Note

This release fixes a regression in 2.13.8 introduced by the fix to mount rule generations.

# Obtaining the Release

There are two ways to obtain this release either through gitlab or a tarball in launchpad. 

**Important note:** the gitlab release tarballs differ from the launchpad release tarballs. The launchpad release tarball has a couple processing steps already performed:

* libapparmor `autogen.sh` is already done, meaning distros only need to use ./configure in their build setup
* the docs for everything but libapparmor have already been built

### gitlab
- https://gitlab.com/apparmor/apparmor/-/releases/v2.13.9

### Launchpad
  -   <https://launchpad.net/apparmor/2.13/2.13.9/+download/apparmor-2.13.9.tar.gz>
  -   sha256sum: 131dc578ba1c7505fa48e70cb2e09f7f9fec43273db0167482b3014393e3a2c6
  -   signature: <https://launchpad.net/apparmor/2.13/2.13.9/+download/apparmor-2.13.9.tar.gz.asc>
  -   sha256sum: 8ac0e08ab2b21af133ca28bae621131dfad12631c93c7a85a83b1653dddf31db
# Changes in this Release

These release notes cover all changes between 2.13.8 (027faf20dd6442f19362aa3deb0c4849358a44fd) and 2.13.9 (2292c7baeb7aa0f9dec352bc40260b575ce556d7) on [apparmor-2.13 branch](https://gitlab.com/apparmor/apparmor/tree/apparmor-2.13).

## Policy Compiler (a.k.a apparmor_parser)
- fix parsing of source as mount point for propagation type flags ([MR:1048](https://gitlab.com/apparmor/apparmor/-/merge_requests/1048), [LP:1648245](https://bugs.launchpad.net/bugs/1648245), [LP:1648245](https://bugs.launchpad.net/bugs/1648245))

## Policy

#### abstractions
- base
  -  allow reading of /etc/ld-musl-*.path ([MR:1047](https://gitlab.com/apparmor/apparmor/-/merge_requests/1047),
    [AABUG:333](https://gitlab.com/apparmor/apparmor/-/issues/333))
- snap_browsers
  - add lock file permission to snap browsers ([MR:1045](https://gitlab.com/apparmor/apparmor/-/merge_requests/1045))
