---
title: AppArmor 3.0.11 released
date: 2023-06-09
---

AppArmor 3.0.11 is a bug fix release of the user space components of the AppArmor security project. The kernel portion of the project is maintained and pushed separately.

This version of the userspace should work with all kernel versions from
2.6.15 and later (some earlier version of the kernel if they have the
apparmor patches applied).

# Important Note

This release fixes a regression in 3.0.10 introduced by the fix to mount rule generations.

# Obtaining the Release

There are two ways to obtain this release either through gitlab or a tarball in launchpad. 

**Important note:** the gitlab release tarballs differ from the launchpad release tarballs. The launchpad release tarball has a couple processing steps already performed:

* libapparmor `autogen.sh` is already done, meaning distros only need to use ./configure in their build setup
* the docs for everything but libapparmor have already been built

### gitlab
- https://gitlab.com/apparmor/apparmor/-/releases/v3.0.11

### Launchpad
  -   <https://launchpad.net/apparmor/3.0/3.0.11/+download/apparmor-3.0.11.tar.gz>
  -   sha256sum: 860608cc8973b8f9774ec4bb796ec927b0756310859f490569633730a8e4a402
  -   signature: <https://launchpad.net/apparmor/3.0/3.0.11/+download/apparmor-3.0.11.tar.gz.asc>
  -   signature sha256sum: dbaf4e11286c16f0a44c8ee5ca01c73d45e7f813a63297aadeb519d17cb58a09

# Changes in this Release

These release notes cover all changes between 3.0.10 (090ed4185ca63220cd492f32612b73386806e5d2) and 3.0.11 (26f177609488c1b5e12ee99d17758221d25a9739) on the [apparmor-3.0 branch](https://gitlab.com/apparmor/apparmor/tree/apparmor-3.0).

## Policy Compiler (a.k.a apparmor_parser)
- fix parsing of source as mount point for propagation type flags ([MR:1048](https://gitlab.com/apparmor/apparmor/-/merge_requests/1048), [LP:1648245](https://bugs.launchpad.net/bugs/1648245), [LP:1648245](https://bugs.launchpad.net/bugs/1648245))
- Fix use-after-free of 'name' ([MR:1040](https://gitlab.com/apparmor/apparmor/-/merge_requests/1040))
- Fix order of if conditions to avoid unreachable code ([MR:1039](https://gitlab.com/apparmor/apparmor/-/merge_requests/1039))

## Utils
- aa-status
  - Fix invalid json output ([MR:1046](https://gitlab.com/apparmor/apparmor/-/merge_requests/1046))

## Policy

#### abstractions
- base
  -  allow reading of /etc/ld-musl-*.path ([MR:1047](https://gitlab.com/apparmor/apparmor/-/merge_requests/1047),
    [AABUG:333](https://gitlab.com/apparmor/apparmor/-/issues/333))
- snap_browsers
  - add lock file permission to snap browsers ([MR:1045](https://gitlab.com/apparmor/apparmor/-/merge_requests/1045))

