---
title: AppArmor 3.1.6 released
date: 2023-06-21
---

AppArmor 3.1.6 is a bug fix release of the user space components of the AppArmor security project. The kernel portion of the project is maintained and pushed separately.

This version of the userspace should work with all kernel versions from
2.6.15 and later (some earlier version of the kernel if they have the
apparmor patches applied).

# Important Note

This release fixes a regression in 3.1.5 introduced by its fix to mount rule generations.

# Obtaining the Release

There are two ways to obtain this release either through gitlab or a tarball in launchpad. 

**Important note:** the gitlab release tarballs differ from the launchpad release tarballs. The launchpad release tarball has a couple processing steps already performed:

* libapparmor `autogen.sh` is already done, meaning distros only need to use ./configure in their build setup
* the docs for everything but libapparmor have already been built

### gitlab
- https://gitlab.com/apparmor/apparmor/-/releases/v3.1.6

### Launchpad
  -   <https://launchpad.net/apparmor/3.1/3.1.6/+download/apparmor-3.1.6.tar.gz>
  -   sha256sum: d5d699fd43faffd924dd51bfb5781a5a7cbabb55c1c9cb4abfb8c2840a9e8fcd
  -   signature: <https://launchpad.net/apparmor/3.1/3.1.6/+download/apparmor-3.1.6.tar.gz.asc>
  -   signature sha256sum: fd68b2a19c3fbbbbe8607fed08521d7ad7b5b84389f7926e6a2e6356a1408c0a

# Changes in this Release

These release notes cover all changes between 3.1.5 (tag v3.1.5) ) and 3.1.6 (tag v3.1.6) on the [apparmor-3.1 branch](https://gitlab.com/apparmor/apparmor/tree/apparmor-3.1).


## Policy Compiler (a.k.a apparmor_parser)
- fix rule flag generation change_mount type rule ([MR:1054](https://gitlab.com/apparmor/apparmor/-/merge_requests/1054), [BOO:1211989](https://bugzilla.opensuse.org/show_bug.cgi?id=1211989), Fixes: [LP:2023814](https://bugs.launchpad.net/bugs/2023814))


## Policy

#### abstractions
- base
  - Add transparent hugepage support ([MR:1050](https://gitlab.com/apparmor/apparmor/-/merge_requests/1050))
- authentication
  -  Add GSSAPI mechanism modules config ([MR:1049](https://gitlab.com/apparmor/apparmor/-/merge_requests/1049))
